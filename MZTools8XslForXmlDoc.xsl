﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html" encoding="utf-8" indent="no" />
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Main Template
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="/">
      <html>
         <head>
            <title>
               <xsl:value-of select="'Documentation'" />
            </title>
            <style>
               li                     { margin-top: 10 }
               body                   { font-size: 75%; font-family: verdana, helvetica, arial, sans-serif }
               tbody                  { font-size: 75%; font-family: verdana, helvetica, arial, sans-serif }
               .header1               { font-weight: bold; color: #000080 }
               .header2               { font-weight: bold; color: #000080 }
               .header3               { font-weight: bold; color: #000080 }
               .header4               { font-weight: bold; color: #000080 }
               .Indentation           { position:relative;left:25px;display:block }
               .InvisibleIndentation  { position:relative;left:25px;display:none }
               .Bookmark              { text-decoration: none; color: #000080 }
               .Table                 { border-collapse: collapse; margin-top: 10; }
               .Cell                  { border-left: 1px solid #000000; border-right: 1px solid #000000; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-collapse: collapse; padding-left: 5px; padding-right: 5px }
               .TableHeaderRow        { border-left: 1px solid #000000; border-right: 1px solid #000000; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-collapse: collapse; padding-left: 10px; padding-right: 10px; color: #FFFFFF; background-color: #000080 }
               .PropertyName          { font-weight: bold }
               .ListHeader            { font-weight: bold }
               .Toggle                { width:12 ; height:10 ; border-left: 1px solid #000000; border-right: 1px solid #000000; border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-collapse: collapse; text-align: center; cursor: pointer}
               .ToggleExternalCell    { vertical-align:baseline }
               .ToggleTBody           { font-size: 7pt }
            </style>
            <script type="text/javascript">
               <![CDATA[

            function ToggleElementVisibility(element) {

               elementParent = element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
               
               elementDIV = elementParent.nextSibling
              
               element.cursor = "wait"               
               
               if (element.innerHTML == "-") {
                  element.innerHTML = "+"
                  element.style.width = element.style.height
                  elementDIV.style.display = "none"
               }
               else {
                  element.innerHTML = "-";
                  element.style.width = element.style.height
                  elementDIV.style.display = "block"
               }
               element.cursor = "pointer"               

            }

            ]]>
            </script>
         </head>
         <body>
            <!-- This template applies to Project Group level documentation only -->
            <xsl:apply-templates select="Solution" />
            <xsl:apply-templates select="ProjectGroup" />
            <!-- This template applies to Project level documentation only -->
            <xsl:apply-templates select="Projects" />
            <!-- This template applies to File level documentation only -->
            <xsl:apply-templates select="Files" />
         </body>
      </html>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
         Template for Solution (VS .NET) or ProjectGruoup (VB Classic)
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Solution">
      <div>
         <table>
            <tr>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header1">Solution <xsl:value-of select="Properties/Name" /></span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <!-- Build some properties of the solution -->
         <span class='PropertyName'>Filename: </span>
         <xsl:value-of select="Properties/FileName" />
         <br />
         <br />
         <xsl:apply-templates select="Projects" />
      </div>
   </xsl:template>
 
   <xsl:template match="ProjectGroup">
      <div>
         <table>
            <tr>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header1">Project Group <xsl:value-of select="Properties/Name" /></span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <!-- Build some properties of the project group -->
         <span class='PropertyName'>Filename: </span>
         <xsl:value-of select="Properties/FileName" />
         <br />
         <br />
         <xsl:apply-templates select="Projects" />
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Projects
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Projects">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Projects:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Name</th>
               <th class="TableHeaderRow">Filename</th>
               <th class="TableHeaderRow">Output Type</th>
            </tr>
            <xsl:for-each select="Project">
               <xsl:sort select="Properties/OutputType" />
               <xsl:sort select="Properties/Name" />
               <tr>
                  <td class="Cell">
                     <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                     <!-- We add a hyperlink to a bookmark with information about the project -->
                     <a class="Bookmark">
                        <xsl:attribute name="href">
                           <xsl:text>#</xsl:text>
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FileName" />
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="Properties/Name" />
                     </a>
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/FileName" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/OutputType" />
                  </td>
               </tr>
            </xsl:for-each>
         </table>
         <br />
         <!-- Build the documentation section for each project -->
         <xsl:for-each select="Project">
            <xsl:sort select="Properties/OutputType" />
            <xsl:sort select="Properties/Name" />
            <!-- Apply template for Project -->
            <xsl:apply-templates select="." />
         </xsl:for-each>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Project
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Project">
      <!-- Build the header -->
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header2">
                     <!-- Build a bookmark -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FileName" />
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>
                     <xsl:text>Project </xsl:text>
                     <xsl:value-of select="Properties/Name" />
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <!-- Build some properties of the project-->
         <span class='PropertyName'>Filename: </span>
         <xsl:value-of select="Properties/FileName" />
         <br />
         <span class='PropertyName'>Language: </span>
         <xsl:value-of select="Properties/LanguageDescription" />
         <br />
         <span class='PropertyName'>Output Type: </span>
         <xsl:value-of select="Properties/OutputType" />
         <br />
         <br />
         <!-- Build the properties table -->
         <xsl:apply-templates select="Properties" />
         <!-- Build the references table -->
         <xsl:if test="count(References/Reference) != 0">
            <xsl:apply-templates select="References" />
         </xsl:if>
         <!-- Build the web references table -->
         <xsl:if test="count(WebReferences/WebReference) != 0">
            <xsl:apply-templates select="WebReferences" />
         </xsl:if>
         <!-- Build the imports table -->
         <xsl:if test="count(Imports/Import) != 0">
            <xsl:apply-templates select="Imports" />
         </xsl:if>
         <!-- Build the files table (Non Enterprise Projects) -->
         <xsl:if test="count(Files/File) != 0">
            <xsl:apply-templates select="Files" />
         </xsl:if>
         <!-- Build the projects table (Enterprise Projects) -->
         <xsl:if test="count(Projects/Project) != 0">
            <xsl:apply-templates select="Projects" />
         </xsl:if>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for References
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="References">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>References:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Name</th>
               <th class="TableHeaderRow">Filename</th>
               <th class="TableHeaderRow">Description</th>
               <th class="TableHeaderRow">Type</th>
               <th class="TableHeaderRow">Version</th>
            </tr>
            <xsl:for-each select="Reference">
               <tr>
                  <td class="Cell">
	                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                     <xsl:value-of select="Properties/Name" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/FileName" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/Description" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/Type" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/Version" />
                  </td>
               </tr>
            </xsl:for-each>
         </table>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Web References
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="WebReferences">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Web References:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Folder Name</th>
               <th class="TableHeaderRow">Web Reference</th>
               <th class="TableHeaderRow">URL Behavior</th>
            </tr>
            <xsl:for-each select="WebReference">
               <tr>
                  <td class="Cell">
                     <xsl:value-of select="Properties/FileName" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/WebReference" />
                  </td>
                  <td class="Cell">
                     <xsl:if test="Properties/UrlBehavior = '0'">Static</xsl:if>
                     <xsl:if test="Properties/UrlBehavior = '1'">Dynamic</xsl:if>
                  </td>
               </tr>
            </xsl:for-each>
         </table>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Imports
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Imports">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Imports:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Import</th>
            </tr>
            <xsl:for-each select="Import">
               <tr>
                  <td class="Cell">
                     <xsl:value-of select="." />
                  </td>
               </tr>
            </xsl:for-each>
         </table>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Files
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Files">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Files:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Filename</th>
               <th class="TableHeaderRow">Type</th>
            </tr>
            <xsl:for-each select="File">
               <xsl:sort select="Properties/SubType" />
               <xsl:sort select="Properties/FileName" />
               <tr>
                  <td class="Cell">
                     <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                     <!-- We add a hyperlink to a bookmark with information about the file -->
                     <a class="Bookmark">
                        <xsl:attribute name="href">
                           <xsl:text>#</xsl:text>
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <!-- The FullPath is empty for VBA files of a VBA project, so we add also the FileName -->
                                 <xsl:value-of select="Properties/FileName" />
                                 <xsl:value-of select="Properties/FullPath" />
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="Properties/FileName" />
                     </a>
                  </td>
                  <td class="Cell">
                     <xsl:choose>
                        <xsl:when test="count(Properties/SubType) = 0">Other</xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="Properties/SubType" />
                        </xsl:otherwise>
                     </xsl:choose>
                  </td>
               </tr>
            </xsl:for-each>
         </table>
         <br />
         <!-- Build the documentation section for each file -->
         <xsl:for-each select="File">
            <xsl:sort select="Properties/SubType" />
            <xsl:sort select="Properties/FileName" />
            <!-- Apply template for File -->
            <xsl:apply-templates select="." />
         </xsl:for-each>
         <br />
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for File
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="File">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton">
                  <xsl:with-param name="Expanded">False</xsl:with-param>
               </xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header3">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <!-- The FullPath is empty for VBA files of a VBA project, so we add also the FileName -->
                                 <xsl:value-of select="Properties/FileName" />
                                 <xsl:value-of select="Properties/FullPath" />
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>
                     <xsl:text>File </xsl:text>
                     <xsl:value-of select="Properties/FileName" />
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="InvisibleIndentation">
         <xsl:apply-templates select="Properties" />
         <xsl:if test="count(Designer) > 0">
            <xsl:apply-templates select="Designer">
               <xsl:with-param name="FileFullName">
                  <xsl:value-of select="Properties/FullPath" />
               </xsl:with-param>
            </xsl:apply-templates>
         </xsl:if>
         <xsl:call-template name="FileAttributes" />
         <xsl:apply-templates select="CodeElements">
            <xsl:with-param name="FileFullName">
               <xsl:value-of select="Properties/FullPath" />
            </xsl:with-param>
         </xsl:apply-templates>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Designer
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Designer">
      <xsl:param name="FileFullName"></xsl:param>
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Designer</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <xsl:apply-templates select="Properties" />
         <xsl:apply-templates select="Components">
            <xsl:with-param name="FileFullName">
               <xsl:value-of select="$FileFullName"></xsl:value-of>
            </xsl:with-param>
         </xsl:apply-templates>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Components
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Components">
      <xsl:param name="FileFullName"></xsl:param>
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Components:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:choose>
            <xsl:when test="count(Component) = 0">
               <br />None
            </xsl:when>
            <xsl:otherwise>
               <table class="Table">
                  <tr>
                     <th class="TableHeaderRow">Name</th>
                     <th class="TableHeaderRow">Type</th>
                     <th class="TableHeaderRow">Text/Caption</th>
                  </tr>
                  <xsl:for-each select="Component">
                     <xsl:sort select="Properties/Name" />
                     <tr>
                        <td class="Cell">
                           <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                           <a class="Bookmark">
                              <xsl:attribute name="href">
                                 <xsl:text>#</xsl:text>
                                 <xsl:call-template name="BookmarkName">
                                    <xsl:with-param name="BookmarkFullName">
                                       <!-- We must include the file fullname in the bookmark because otherwise the component name wouldn't be unique -->
                                       <xsl:value-of select="$FileFullName" />
                                       <xsl:value-of select="Properties/Name" />
                                    </xsl:with-param>
                                 </xsl:call-template>
                              </xsl:attribute>
                              <xsl:value-of select="Properties/Name" />
                           </a>
                        </td>
                        <td class="Cell">
                           <xsl:value-of select="Properties/Type" />
                        </td>
                        <td class="Cell">
                           <xsl:value-of select="Properties/Text" />
                           <xsl:value-of select="Properties/Caption" />
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
               <br />
               <!-- Build the documentation section for each component -->
               <xsl:for-each select="Component">
                  <xsl:sort select="Properties/Name" />
                  <!-- Apply template for Component -->
                  <xsl:apply-templates select=".">
                     <xsl:with-param name="FileFullName">
                        <xsl:value-of select="$FileFullName"></xsl:value-of>
                     </xsl:with-param>
                  </xsl:apply-templates>
               </xsl:for-each>
               <br />
            </xsl:otherwise>
         </xsl:choose>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Component
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Component">
      <xsl:param name="FileFullName"></xsl:param>
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton">
                  <xsl:with-param name="Expanded">False</xsl:with-param>
               </xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header3">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="$FileFullName"></xsl:value-of>
                                 <xsl:value-of select="Properties/Name" />
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>
                     <xsl:text>Component </xsl:text>
                     <xsl:value-of select="Properties/Name" />
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="InvisibleIndentation">
         <xsl:apply-templates select="Properties" />
      </div>
   </xsl:template>
   
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for FileAttributes
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="FileAttributes">
      <xsl:if test="count(CodeElements/CodeElement[Properties/Kind = 'Attribute' ]) != 0">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Attributes:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Name</th>
               <th class="TableHeaderRow">Declaration</th>
            </tr>
            <xsl:for-each select="CodeElements/CodeElement[Properties/Kind = 'Attribute' ]">
               <xsl:sort select="Properties/Name" />
               <tr>
                  <td class="Cell">
                     <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                     <xsl:value-of select="Properties/Name" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Properties/Declaration" />
                  </td>
               </tr>
            </xsl:for-each>
         </table>
      </div>
      <br />
      </xsl:if>
   </xsl:template>
      
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElements
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="CodeElements">
      <xsl:param name="FileFullName"></xsl:param>
      
      <xsl:call-template name="Events" />
      <xsl:call-template name="CodeElementsClassesModulesInterfacesStructs">
         <xsl:with-param name="CodeElementKind">Class</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="CodeElementsClassesModulesInterfacesStructs">
         <xsl:with-param name="CodeElementKind">Module</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="CodeElementsClassesModulesInterfacesStructs">
         <xsl:with-param name="CodeElementKind">Struct</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="CodeElementsClassesModulesInterfacesStructs">
         <xsl:with-param name="CodeElementKind">Interface</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="CodeElementsConstantsFields">
         <xsl:with-param name="CodeElementKind">Constant</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="CodeElementsConstantsFields">
         <xsl:with-param name="CodeElementKind">Field</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="CodeElementsDelegates" />
      <xsl:call-template name="CodeElementsEnums" />
      <xsl:call-template name="CodeElementsMethods" />
      <xsl:call-template name="CodeElementsNamespaces">
         <xsl:with-param name="FileFullName">
            <xsl:value-of select="$FileFullName"></xsl:value-of>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementsNamespaces
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementsNamespaces">
      <xsl:param name="FileFullName"></xsl:param>
      <xsl:if test="count(CodeElement[Properties/Kind = 'Namespace' ]) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Namespaces:</span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Name</th>
               </tr>
               <xsl:for-each select="CodeElement[Properties/Kind = 'Namespace' ]">
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <!-- We add a hyperlink to a bookmark with information about the namespace -->
                        <a class="Bookmark">
                           <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                              <xsl:call-template name="BookmarkName">
                                 <xsl:with-param name="BookmarkFullName">
                                    <!-- We must include the file fullname in the bookmark because in C# the namespace
                                    appears in each file -->
                                    <xsl:value-of select="$FileFullName" />
                                    <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                 </xsl:with-param>
                              </xsl:call-template>
                           </xsl:attribute>
                           <xsl:value-of select="Properties/Name"></xsl:value-of>
                        </a>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
            <xsl:for-each select="CodeElement[Properties/Kind = 'Namespace']">
               <xsl:call-template name="CodeElementNamespace">
                  <xsl:with-param name="FileFullName">
                     <xsl:value-of select="$FileFullName"></xsl:value-of>
                  </xsl:with-param>
               </xsl:call-template>
            </xsl:for-each>
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementNamespace
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementNamespace">
      <xsl:param name="FileFullName"></xsl:param>
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header4">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="$FileFullName"></xsl:value-of>
                                 <xsl:value-of select="Properties/FullName"></xsl:value-of>
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>
                     <xsl:text>Namespace </xsl:text>
                     <xsl:value-of select="Properties/Name"></xsl:value-of>
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:apply-templates select="CodeElements">
            <xsl:with-param name="FileFullName">
               <xsl:value-of select="$FileFullName"></xsl:value-of>
            </xsl:with-param>
         </xsl:apply-templates>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
        Template for CodeElement Classes, Modules, Interfaces, Structs
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementsClassesModulesInterfacesStructs">
      <xsl:param name="CodeElementKind"></xsl:param>
      <xsl:if test="count(CodeElement[Properties/Kind = $CodeElementKind]) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>
                        <xsl:if test="$CodeElementKind = 'Class'">Classes:</xsl:if>
                        <xsl:if test="$CodeElementKind = 'Module'">Modules:</xsl:if>
                        <xsl:if test="$CodeElementKind = 'Struct'">Structures:</xsl:if>
                        <xsl:if test="$CodeElementKind = 'Interface'">Interfaces:</xsl:if>
                     </span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Access</th>
               </tr>
               <xsl:for-each select="CodeElement[Properties/Kind = $CodeElementKind ]">
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <!-- We add a hyperlink to a bookmark with information about the class -->
                        <a class="Bookmark">
                           <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                              <xsl:call-template name="BookmarkName">
                                 <xsl:with-param name="BookmarkFullName">
                                    <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                 </xsl:with-param>
                              </xsl:call-template>
                           </xsl:attribute>
                           <xsl:value-of select="Properties/Name"></xsl:value-of>
                        </a>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Access"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
            <xsl:for-each select="CodeElement[Properties/Kind = $CodeElementKind]">
               <xsl:call-template name="CodeElementClassModuleInterfaceStruct">
                  <xsl:with-param name="CodeElementKind">
                     <xsl:value-of select="$CodeElementKind" />
                  </xsl:with-param>
               </xsl:call-template>
            </xsl:for-each>
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
      Template for CodeElement Class, Module, Interface or Struct
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementClassModuleInterfaceStruct">
      <xsl:param name="CodeElementKind"></xsl:param>
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header4">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FullName"></xsl:value-of>
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>
                     <xsl:value-of select="Properties/Kind"></xsl:value-of>
                     <xsl:text> </xsl:text>
                     <xsl:value-of select="Properties/Name"></xsl:value-of>
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <span class='PropertyName'>Name: </span>
         <xsl:value-of select="Properties/Name"></xsl:value-of>
         <br />
         <span class='PropertyName'>Access: </span>
         <xsl:value-of select="Properties/Access"></xsl:value-of>
         <br />
         <br />
         <xsl:apply-templates select="Properties/Declaration"></xsl:apply-templates>
         <xsl:if test="Properties/Kind = 'Class' ">
            <xsl:apply-templates select="Bases"></xsl:apply-templates>
            <xsl:apply-templates select="ImplementedInterfaces"></xsl:apply-templates>
         </xsl:if>
         <xsl:if test="Properties/Kind = 'Struct' ">
            <xsl:apply-templates select="ImplementedInterfaces"></xsl:apply-templates>
         </xsl:if>
         <xsl:if test="Properties/Kind = 'Interface' ">
            <xsl:apply-templates select="Bases"></xsl:apply-templates>
         </xsl:if>
         <xsl:apply-templates select="CommentLines"></xsl:apply-templates>
         <xsl:apply-templates select="Properties"></xsl:apply-templates>
         <xsl:apply-templates select="Attributes" />
         <xsl:apply-templates select="CodeElements">
            <xsl:with-param name="FileFullName">
               <!-- No file name required -->
            </xsl:with-param>
         </xsl:apply-templates>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementsEnums
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementsEnums">
      <xsl:if test="count(CodeElement[Properties/Kind = 'Enum' ] ) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Enums:</span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Access</th>
               </tr>
               <xsl:for-each select="CodeElement[Properties/Kind = 'Enum' ]">
                  <xsl:sort select="Properties/Name" />
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <a class="Bookmark">
                           <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                              <xsl:call-template name="BookmarkName">
                                 <xsl:with-param name="BookmarkFullName">
                                    <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                 </xsl:with-param>
                              </xsl:call-template>
                           </xsl:attribute>
                           <xsl:value-of select="Properties/Name"></xsl:value-of>
                        </a>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Access"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
            <xsl:for-each select="CodeElement[Properties/Kind = 'Enum' ]">
               <xsl:sort select="Properties/Name" />
               <xsl:call-template name="CodeElementEnum" />
            </xsl:for-each>
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementEnum
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementEnum">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header4">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FullName"></xsl:value-of>
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>Enum <xsl:value-of select="Properties/Name"></xsl:value-of>
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <span class='PropertyName'>Name: </span>
         <xsl:value-of select="Properties/Name"></xsl:value-of>
         <br />
         <span class='PropertyName'>Access: </span>
         <xsl:value-of select="Properties/Access"></xsl:value-of>
         <br />
         <br />
         <xsl:apply-templates select="Properties/Declaration"></xsl:apply-templates>
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Values:</span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <br />
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Value</th>
               </tr>
               <xsl:for-each select="CodeElements/CodeElement[Properties/Kind='Enum Item']">
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <xsl:value-of select="Properties/Name"></xsl:value-of>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/InitExpression"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
         </div>
         <br />
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementsConstantsFields
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementsConstantsFields">
      <xsl:param name="CodeElementKind"></xsl:param>
      <xsl:if test="count(CodeElement[Properties/Kind = $CodeElementKind]) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>
                        <xsl:if test="$CodeElementKind = 'Constant'">Constants:</xsl:if>
                        <xsl:if test="$CodeElementKind = 'Field'">Fields:</xsl:if>
                     </span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Access</th>
                  <th class="TableHeaderRow">Declaration</th>
               </tr>
               <xsl:for-each select="CodeElement[Properties/Kind = $CodeElementKind ]">
                  <xsl:sort select="Properties/Name" />
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <xsl:value-of select="Properties/Name"></xsl:value-of>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Access"></xsl:value-of>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Declaration"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementsDelegates
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementsDelegates">
      <xsl:if test="count(CodeElement[Properties/Kind = 'Delegate' ] ) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Delegates:</span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Kind</th>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Access</th>
               </tr>
               <xsl:for-each select="CodeElement[Properties/Kind = 'Delegate' ]">
                  <xsl:sort select="Properties/Name" />
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <xsl:value-of select="Properties/MethodKind"></xsl:value-of>
                     </td>
                     <td class="Cell">
                        <a class="Bookmark">
                           <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                              <xsl:call-template name="BookmarkName">
                                 <xsl:with-param name="BookmarkFullName">
                                    <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                 </xsl:with-param>
                              </xsl:call-template>
                           </xsl:attribute>
                           <xsl:value-of select="Properties/Name"></xsl:value-of>
                        </a>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Access"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
            <xsl:for-each select="CodeElement[Properties/Kind = 'Delegate' ]">
               <xsl:sort select="Properties/Name" />
               <xsl:call-template name="CodeElementDelegate" />
            </xsl:for-each>
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementDelegate
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementDelegate">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header4">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FullName"></xsl:value-of>
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>Delegate <xsl:value-of select="Properties/Name"></xsl:value-of>
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <span class='PropertyName'>Kind: </span>
         <xsl:value-of select="Properties/MethodKind"></xsl:value-of>
         <br />
         <span class='PropertyName'>Name: </span>
         <xsl:value-of select="Properties/Name"></xsl:value-of>
         <br />
         <span class='PropertyName'>Access: </span>
         <xsl:value-of select="Properties/Access"></xsl:value-of>
         <br />
         <br />
         <xsl:apply-templates select="Properties/Declaration"></xsl:apply-templates>
         <xsl:apply-templates select="CommentLines"></xsl:apply-templates>
         <xsl:apply-templates select="Properties"></xsl:apply-templates>
         <xsl:apply-templates select="Attributes" />
         <xsl:apply-templates select="Parameters"></xsl:apply-templates>
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementsMethods
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementsMethods">
      <xsl:if test="count(CodeElement
         [
            Properties/Kind = 'Method' or 
            Properties/Kind = 'Property'
         ] ) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Methods:</span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Kind</th>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Access</th>
               </tr>
               <xsl:for-each select="CodeElement
                  [
                     Properties/Kind = 'Method' or 
                     Properties/Kind = 'Property' 
                  ]">
                  <xsl:sort select="Properties/Name" />
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <xsl:choose>
                           <xsl:when test="Properties/Kind = 'Property'">Property</xsl:when>
                           <xsl:otherwise>
                              <xsl:value-of select="Properties/MethodKind"></xsl:value-of>
                           </xsl:otherwise>
                        </xsl:choose>
                     </td>
                     <td class="Cell">
                        <a class="Bookmark">
                           <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                              <xsl:call-template name="BookmarkName">
                                 <xsl:with-param name="BookmarkFullName">
                                    <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                    <!-- We must include also the signature in the bookmark to take
                                    into account overloaded methods -->
                                    <xsl:value-of select="Properties/Declaration"></xsl:value-of>
                                 </xsl:with-param>
                              </xsl:call-template>
                           </xsl:attribute>
                           <xsl:value-of select="Properties/Name"></xsl:value-of>
                        </a>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Access"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
            <xsl:for-each select="CodeElement
               [
                  Properties/Kind = 'Method' or 
                  Properties/Kind = 'Property' 
               ]">
               <xsl:sort select="Properties/Name" />
               <xsl:call-template name="CodeElementMethod" />
            </xsl:for-each>
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for CodeElementMethod
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="CodeElementMethod">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header4">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                 <xsl:value-of select="Properties/Declaration"></xsl:value-of>
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>
                     <xsl:choose>
                        <xsl:when test="Properties/Kind = 'Property'">Property </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="Properties/MethodKind"></xsl:value-of>
                           <xsl:text> </xsl:text>
                        </xsl:otherwise>
                     </xsl:choose>
                     <xsl:value-of select="Properties/Name"></xsl:value-of>
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <span class='PropertyName'>Kind: </span>
         <xsl:choose>
            <xsl:when test="Properties/Kind = 'Property'">Property</xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="Properties/MethodKind"></xsl:value-of>
            </xsl:otherwise>
         </xsl:choose>
         <br />
         <span class='PropertyName'>Name: </span>
         <xsl:value-of select="Properties/Name"></xsl:value-of>
         <br />
         <span class='PropertyName'>Access: </span>
         <xsl:value-of select="Properties/Access"></xsl:value-of>
         <br />
         <br />
         <xsl:apply-templates select="Properties/Declaration"></xsl:apply-templates>
         <xsl:apply-templates select="CommentLines"></xsl:apply-templates>
         <xsl:apply-templates select="Properties"></xsl:apply-templates>
         <xsl:apply-templates select="Parameters"></xsl:apply-templates>
         <xsl:apply-templates select="Attributes" />
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Events
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="Events">
      <xsl:if test="count(CodeElement[Properties/Kind = 'Event'] ) != 0">
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Events:</span>
                  </td>
               </tr>
            </table>
         </div>
         <div class="Indentation">
            <table class="Table">
               <tr>
                  <th class="TableHeaderRow">Name</th>
                  <th class="TableHeaderRow">Access</th>
               </tr>
               <xsl:for-each select="CodeElement[Properties/Kind = 'Event']">
                  <xsl:sort select="Properties/Name" />
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                        <a class="Bookmark">
                           <xsl:attribute name="href">
                              <xsl:text>#</xsl:text>
                              <xsl:call-template name="BookmarkName">
                                 <xsl:with-param name="BookmarkFullName">
                                    <xsl:value-of select="Properties/FullName"></xsl:value-of>
                                 </xsl:with-param>
                              </xsl:call-template>
                           </xsl:attribute>
                           <xsl:value-of select="Properties/Name"></xsl:value-of>
                        </a>
                     </td>
                     <td class="Cell">
                        <xsl:value-of select="Properties/Access"></xsl:value-of>
                     </td>
                  </tr>
               </xsl:for-each>
            </table>
            <br />
            <xsl:for-each select="CodeElement[Properties/Kind = 'Event']">
               <xsl:sort select="Properties/Name" />
               <xsl:call-template name="Event" />
            </xsl:for-each>
         </div>
         <br />
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Event
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="Event">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                  <span class="header4">
                     <!-- Build a bookmark name -->
                     <a>
                        <xsl:attribute name="name">
                           <xsl:call-template name="BookmarkName">
                              <xsl:with-param name="BookmarkFullName">
                                 <xsl:value-of select="Properties/FullName"></xsl:value-of>
                              </xsl:with-param>
                           </xsl:call-template>
                        </xsl:attribute>
                     </a>Event <xsl:value-of select="Properties/Name"></xsl:value-of>
                  </span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <span class='PropertyName'>Name: </span>
         <xsl:value-of select="Properties/Name"></xsl:value-of>
         <br />
         <span class='PropertyName'>Access: </span>
         <xsl:value-of select="Properties/Access"></xsl:value-of>
         <br />
         <br />
         <xsl:apply-templates select="Properties/Declaration"></xsl:apply-templates>
         <xsl:apply-templates select="CommentLines"></xsl:apply-templates>
         <xsl:apply-templates select="Properties"></xsl:apply-templates>
         <xsl:apply-templates select="Parameters"></xsl:apply-templates>
         <xsl:apply-templates select="Attributes" />
      </div>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Events
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Events">
      <!-- If there is at least one element -->
      <xsl:if test="Event != '' ">
         <br />
         <div>
            <table>
               <tr>
                  <xsl:call-template name="ToggleButton"></xsl:call-template>
                  <td>
                     <span class='ListHeader'>Events:</span>
                  </td>
               </tr>
            </table>
         </div>
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Name</th>
               <th class="TableHeaderRow">Declaration</th>
            </tr>
            <xsl:for-each select="Event">
               <tr>
                  <td class="Cell">
                     <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                     <xsl:value-of select="Name" />
                  </td>
                  <td class="Cell">
                     <xsl:value-of select="Declaration" />
                  </td>
               </tr>
            </xsl:for-each>
         </table>
      </xsl:if>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Bases
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Bases">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Bases:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:choose>
            <xsl:when test="count(Base) = 0">
               <br />None
            </xsl:when>
            <xsl:otherwise>
               <table class="Table">
                  <tr>
                     <th class="TableHeaderRow">Name</th>
                  </tr>
                  <xsl:for-each select="Base">
                     <tr>
                        <td class="Cell">
                           <xsl:value-of select="Properties/FullName" />
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
            </xsl:otherwise>
         </xsl:choose>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Implemented Interfaces
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="ImplementedInterfaces">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Implemented Interfaces:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:choose>
            <xsl:when test="count(ImplementedInterface) = 0">
               <br />None
            </xsl:when>
            <xsl:otherwise>
               <table class="Table">
                  <tr>
                     <th class="TableHeaderRow">Name</th>
                  </tr>
                  <xsl:for-each select="ImplementedInterface">
                     <tr>
                        <td class="Cell">
                           <xsl:value-of select="Properties/FullName" />
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
            </xsl:otherwise>
         </xsl:choose>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Comments
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="CommentLines">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Comment:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:choose>
            <xsl:when test="count(CommentLine) = 0">
               <br />None
            </xsl:when>
            <xsl:otherwise>
               <br />
               <xsl:for-each select="CommentLine">
                  <xsl:value-of disable-output-escaping = "yes" select="translate(., ' ', '&#160;')" />
                  <br />
               </xsl:for-each>
            </xsl:otherwise>
         </xsl:choose>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Properties
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Properties">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Properties:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <table class="Table">
            <tr>
               <th class="TableHeaderRow">Name</th>
               <th class="TableHeaderRow">Value</th>
            </tr>
            <xsl:for-each select="*">
               <xsl:sort select="name()"></xsl:sort>
               <xsl:if test="(name() !='Name') and (name() != 'Kind') and (name() != 'Access') and (name() != 'Declaration')">
                  <tr>
                     <td class="Cell">
                        <xsl:call-template name="ImageFromFile">
                           <xsl:with-param name="FileName">Image0.png</xsl:with-param>
                        </xsl:call-template>
                        <xsl:value-of select="name()" />
                     </td>
                     <td class="Cell">
                         <xsl:value-of select="." />
                     </td>
                  </tr>
               </xsl:if>
            </xsl:for-each>
         </table>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Declaration
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Declaration">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Declaration:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <br />
         <xsl:value-of select="."></xsl:value-of>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Attributes
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Attributes">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Attributes:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:choose>
            <xsl:when test="count(Attribute) = 0">
               <br />None
            </xsl:when>
            <xsl:otherwise>
               <table class="Table">
                  <tr>
                     <th class="TableHeaderRow">Name</th>
                     <th class="TableHeaderRow">Value</th>
                  </tr>
                  <xsl:for-each select="Attribute">
                     <tr>
                        <td class="Cell">
                           <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                           <xsl:value-of select="Properties/Name" />
                        </td>
                        <td class="Cell">
                           <xsl:value-of select="Properties/Value" />
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
            </xsl:otherwise>
         </xsl:choose>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Parameters
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template match="Parameters">
      <div>
         <table>
            <tr>
               <xsl:call-template name="ToggleButton"></xsl:call-template>
               <td>
                  <span class='ListHeader'>Parameters:</span>
               </td>
            </tr>
         </table>
      </div>
      <div class="Indentation">
         <xsl:choose>
            <xsl:when test="count(Parameter) = 0">
               <br />None
            </xsl:when>
            <xsl:otherwise>
               <table class="Table">
                  <tr>
                     <th class="TableHeaderRow">Name</th>
                     <th class="TableHeaderRow">Type</th>
                     <th class="TableHeaderRow">Modifiers</th>
                  </tr>
                  <xsl:for-each select="Parameter">
                     <tr>
                        <td class="Cell">
                           <xsl:call-template name="ImageFromProperty"></xsl:call-template>
                           <xsl:value-of select="Properties/Name" />
                        </td>
                        <td class="Cell">
                           <xsl:value-of select="Properties/TypeAliasedName" />
                        </td>
                        <td class="Cell">
                           <xsl:value-of select="Properties/Modifiers" />
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
            </xsl:otherwise>
         </xsl:choose>
      </div>
      <br />
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Bookmark
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="BookmarkName">
      <xsl:param name="BookmarkFullName"></xsl:param>
      <!-- Build a bookmark name with the pattern "BookmarkFullName_Bookmark" -->
      <xsl:value-of select="translate($BookmarkFullName,':/\','___')" />
      <xsl:text>_Bookmark</xsl:text>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Template for Toggle Button
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="ToggleButton">
      <xsl:param name="Expanded"></xsl:param>
      <td class="ToggleExternalCell">
         <table>
            <tbody class="ToggleTBody">
               <tr>
                  <td class="Toggle" onclick="ToggleElementVisibility(this)">
                     <xsl:choose>
                        <xsl:when test="$Expanded = 'False'">
                           <xsl:text>+</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:text>-</xsl:text>
                        </xsl:otherwise>
                     </xsl:choose>
                  </td>
               </tr>
            </tbody>
         </table>
      </td>
   </xsl:template>
   <!--
   ////////////////////////////////////////////////////////////////////////
                               Templates for Images
   ////////////////////////////////////////////////////////////////////////
   -->
   <xsl:template name="ImageFromProperty">
      <xsl:call-template name="ImageFromFile">
         <xsl:with-param name="FileName">
            <xsl:value-of select="_ImageFileName" /> 
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="ImageFromFile">
      <xsl:param name="FileName"></xsl:param>
      <img alt="" width="16" height="16" style="vertical-align:text-bottom">
         <xsl:attribute name="src">
         <xsl:text>Images/</xsl:text>
         <xsl:value-of select="$FileName" /> 
         </xsl:attribute>
      </img>
      <xsl:text> </xsl:text>
   </xsl:template>
   
</xsl:stylesheet>

