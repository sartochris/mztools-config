# MZ-Tools-Einstellungen

## Beschreibung
Dieses Repository speichert meine persönlichen Einstellungen für die MZ-Tools. Der Zweck ist, dieselben Einstellungen auf unterschiedlichen Rechnern gleichermaßen verwenden zu können.

Siehe dazu auch [diesen Beitrag auf meiner Homepage](https://www.juengling-edv.de/projektuebergreifend-effizient-arbeiten/).

## Installation
Erstelle einen Clone dieses Projektes in deinem eigenen GitLab-Account. Dadurch hast du
wie ich die Möglichkeit, deine eigenen Einstellungen in der Cloud zu sichern. Wenn du das
nicht willst, kannst du diesen Schritt übergehen.

Beende zunächst alle Programme, die das MZ-Tools-Plugin benutzen.

Lösche dann alle Einstellungen von MZ-Tools unter deinem User, indem du alle Dateien aus dem Verzeichnis  

    %appdata%\MZTools Software\MZTools8\

entfernst.

Nun clone das Git-Repository (meins oder dein eigenes, siehe oben) in das obige Verzeichnis.
Dabei wird automatisch der **master**-Branch ausgecheckt.

Nun öffne deine Office-Applikation, aktiviere den VB-Editor und gehe dort zu den MZ-Tools-Optionen.
Ändere in den *Persönlichen Einstellungen* den Benutzernamen in deinen eigenen. Überprüfe eventuell
alle anderen Einstellungen.
